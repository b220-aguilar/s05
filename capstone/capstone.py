from abc import ABC, abstractclassmethod

# Person class
class Person(ABC):
	@abstractclassmethod
	def get_full_name(self):
		pass

	@abstractclassmethod
	def add_request(self):
		pass
	
	@abstractclassmethod
	def check_request(self):
		pass

	@abstractclassmethod
	def add_user(self):
		pass

#Employee Class
class Employee(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._full_name = first_name +' '+ last_name
		self._email = email
		self._department = department

	# getters
	def get_first_name(self):
		print(f"First Name: {self._first_name}")

	def get_last_name(self):
		print(f"Last Name: {self._last_name}")

	def get_email(self):
		print(f"Email: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	# setters
	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department	

	# methods
	def check_request(self):
		pass

	def add_user(self):
		pass

	def add_request(self):
		return(f"Request has been added")	

	def get_full_name(self):
		return self._full_name	

	def login(self):
		return(f"{self._email} has logged in")

	def logout(self):
		return(f"{self._email} has logged out")

#Team_Lead Class
class Team_Lead(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._full_name = first_name +' '+ last_name
		self._email = email
		self._department = department
		self._members = []

	# getters
	def get_first_name(self):
		print(f"First Name: {self._first_name}")

	def get_last_name(self):
		print(f"Last Name: {self._last_name}")


	def get_email(self):
		print(f"Email: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	def get_members(self):
		return(self._members)	

	# setters
	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department	

	def set_members(self, members):
		self._members = members		

	# methods
	def check_request(self):
		pass

	def add_user(self):
		pass

	def add_request(self):
		pass	

	def get_full_name(self):
		return self._full_name	

	def login(self):
		return(f"{self._email} has logged in")

	def logout(self):
		return(f"{self._email} has logged out")

	def add_member(self, employee_id):
		self._members.append(employee_id)

		

#Admin Class
class Admin(Person):
	def __init__(self, first_name, last_name, email, department):
		super().__init__()
		self._first_name = first_name
		self._last_name = last_name
		self._full_name = first_name +' '+ last_name
		self._email = email
		self._department = department

	# getters
	def get_first_name(self):
		print(f"First Name: {self._first_name}")

	def get_last_name(self):
		print(f"Last Name: {self._last_name}")

	def get_email(self):
		print(f"Email: {self._email}")

	def get_department(self):
		print(f"Department: {self._department}")

	# setters
	def set_first_name(self, first_name):
		self._first_name = first_name

	def set_last_name(self, last_name):
		self._last_name = last_name

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department	

	# methods
	def check_request(self):
		pass

	def add_user(self):
		pass

	def add_request(self):
		pass	

	def get_full_name(self):
		return self._full_name

	def login(self):
		return(f"{self._email} has logged in")

	def logout(self):
		return(f"{self._email} has logged out")

	def add_user(self):
		return(f"User has been added")

#Request Class
class Request():
	def __init__(self, name, requester, date_requested):
		self._name = name
		self._requester = requester
		self._date_requested = date_requested
		self._status = "open"

	# getters	
	def get_name(self):
		print(f"{self._name}")

	def get_requester(self):
		print(f"{self._requester}")

	def get_date_requested(self):
		print(f"{self._date_requested}")	

	def get_status(self):
		print(f"{self._status}")

	# setters	
	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester
		
	def get_date_requested(self, date_requested):
		self._date_requested = date_requested

	def set_status(self, status):
		self._status = status


	# methods
	def update_request(self):
		return(f"Request {self._name} has been updated")	

	def close_request(self):
		return(f"Request {self._name} has been closed")	

	def cancel_request(self):
		return(f"Request {self._name} has been cancelled")	



# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
team_lead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", team_lead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

employee1.get_full_name()
assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert team_lead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

team_lead1.add_member(employee3)
team_lead1.add_member(employee4)
for indiv_emp in team_lead1.get_members():
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())